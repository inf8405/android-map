import sbt._

import Keys._
import AndroidKeys._

object General {

  val apiLevel = SettingKey[Int]("api-level", "Api Level")
  val googleMapsJar = SettingKey[File]("google-maps-jar", "Google Maps JAR path")

  val settings = Defaults.defaultSettings ++ Seq (
    name := "My Android Project",
    version := "0.1",
    versionCode := 0,
    scalaVersion := "2.9.2",
    apiLevel := 15,
    platformName in Android <<= (apiLevel in Android) { _ formatted "android-%d" }
  )
  
  val proguardSettings = Seq (
    useProguard in Android := true
  )

  lazy val fullAndroidSettings =
    General.settings ++
    AndroidProject.androidSettings ++
    TypedResources.settings ++
    proguardSettings ++
    AndroidManifestGenerator.settings ++
    AndroidMarketPublish.settings ++ Seq (
      keyalias in Android := "change-me",
      libraryDependencies += "org.scalatest" %% "scalatest" % "1.8" % "test",
      googleMapsJar <<= (sdkPath in Android, apiLevel in Android) { (path, apiLevel) =>
        (path / "add-ons" / "addon-google_apis-google-%d".format(apiLevel)
        / "libs" / "maps.jar")
      },
      unmanagedJars in Compile <+= googleMapsJar map { jar => Attributed.blank(jar) }
    )
}

object AndroidBuild extends Build {
  lazy val main = Project (
    "MyAndroidProject",
    file("."),
    settings = General.fullAndroidSettings
  )

  lazy val tests = Project (
    "tests",
    file("tests"),
    settings = General.settings ++
               AndroidTest.androidSettings ++
               General.proguardSettings ++ Seq (
      name := "My Android ProjectTests"
    )
  ) dependsOn main
}
